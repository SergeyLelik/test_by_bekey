import 'package:test_by_bekey/model/rating.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'deliver.freezed.dart';
part 'deliver.g.dart';

@freezed
class Deliver with _$Deliver {
  const Deliver._();
  @JsonSerializable(explicitToJson: true)
  const factory Deliver({
    required int id,
    required String title,
    double? price,
    String? description,
    required String category,
    String? image,
    Rating? rating,
  }) = _Deliver;

  factory Deliver.fromJson(Map<String, dynamic> json) =>
      _$DeliverFromJson(json);
}
