// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'deliver.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Deliver _$$_DeliverFromJson(Map<String, dynamic> json) => _$_Deliver(
      id: json['id'] as int,
      title: json['title'] as String,
      price: (json['price'] as num?)?.toDouble(),
      description: json['description'] as String?,
      category: json['category'] as String,
      image: json['image'] as String?,
      rating: json['rating'] == null
          ? null
          : Rating.fromJson(json['rating'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_DeliverToJson(_$_Deliver instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'price': instance.price,
      'description': instance.description,
      'category': instance.category,
      'image': instance.image,
      'rating': instance.rating?.toJson(),
    };
