// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'deliver_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$DeliverEventTearOff {
  const _$DeliverEventTearOff();

  DeliverStartEvent start() {
    return const DeliverStartEvent();
  }

  DeliverLoadedEvent loaded() {
    return const DeliverLoadedEvent();
  }
}

/// @nodoc
const $DeliverEvent = _$DeliverEventTearOff();

/// @nodoc
mixin _$DeliverEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() start,
    required TResult Function() loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? start,
    TResult Function()? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? start,
    TResult Function()? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DeliverStartEvent value) start,
    required TResult Function(DeliverLoadedEvent value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DeliverStartEvent value)? start,
    TResult Function(DeliverLoadedEvent value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DeliverStartEvent value)? start,
    TResult Function(DeliverLoadedEvent value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeliverEventCopyWith<$Res> {
  factory $DeliverEventCopyWith(
          DeliverEvent value, $Res Function(DeliverEvent) then) =
      _$DeliverEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$DeliverEventCopyWithImpl<$Res> implements $DeliverEventCopyWith<$Res> {
  _$DeliverEventCopyWithImpl(this._value, this._then);

  final DeliverEvent _value;
  // ignore: unused_field
  final $Res Function(DeliverEvent) _then;
}

/// @nodoc
abstract class $DeliverStartEventCopyWith<$Res> {
  factory $DeliverStartEventCopyWith(
          DeliverStartEvent value, $Res Function(DeliverStartEvent) then) =
      _$DeliverStartEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$DeliverStartEventCopyWithImpl<$Res>
    extends _$DeliverEventCopyWithImpl<$Res>
    implements $DeliverStartEventCopyWith<$Res> {
  _$DeliverStartEventCopyWithImpl(
      DeliverStartEvent _value, $Res Function(DeliverStartEvent) _then)
      : super(_value, (v) => _then(v as DeliverStartEvent));

  @override
  DeliverStartEvent get _value => super._value as DeliverStartEvent;
}

/// @nodoc

class _$DeliverStartEvent implements DeliverStartEvent {
  const _$DeliverStartEvent();

  @override
  String toString() {
    return 'DeliverEvent.start()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is DeliverStartEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() start,
    required TResult Function() loaded,
  }) {
    return start();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? start,
    TResult Function()? loaded,
  }) {
    return start?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? start,
    TResult Function()? loaded,
    required TResult orElse(),
  }) {
    if (start != null) {
      return start();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DeliverStartEvent value) start,
    required TResult Function(DeliverLoadedEvent value) loaded,
  }) {
    return start(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DeliverStartEvent value)? start,
    TResult Function(DeliverLoadedEvent value)? loaded,
  }) {
    return start?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DeliverStartEvent value)? start,
    TResult Function(DeliverLoadedEvent value)? loaded,
    required TResult orElse(),
  }) {
    if (start != null) {
      return start(this);
    }
    return orElse();
  }
}

abstract class DeliverStartEvent implements DeliverEvent {
  const factory DeliverStartEvent() = _$DeliverStartEvent;
}

/// @nodoc
abstract class $DeliverLoadedEventCopyWith<$Res> {
  factory $DeliverLoadedEventCopyWith(
          DeliverLoadedEvent value, $Res Function(DeliverLoadedEvent) then) =
      _$DeliverLoadedEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$DeliverLoadedEventCopyWithImpl<$Res>
    extends _$DeliverEventCopyWithImpl<$Res>
    implements $DeliverLoadedEventCopyWith<$Res> {
  _$DeliverLoadedEventCopyWithImpl(
      DeliverLoadedEvent _value, $Res Function(DeliverLoadedEvent) _then)
      : super(_value, (v) => _then(v as DeliverLoadedEvent));

  @override
  DeliverLoadedEvent get _value => super._value as DeliverLoadedEvent;
}

/// @nodoc

class _$DeliverLoadedEvent implements DeliverLoadedEvent {
  const _$DeliverLoadedEvent();

  @override
  String toString() {
    return 'DeliverEvent.loaded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is DeliverLoadedEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() start,
    required TResult Function() loaded,
  }) {
    return loaded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? start,
    TResult Function()? loaded,
  }) {
    return loaded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? start,
    TResult Function()? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DeliverStartEvent value) start,
    required TResult Function(DeliverLoadedEvent value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DeliverStartEvent value)? start,
    TResult Function(DeliverLoadedEvent value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DeliverStartEvent value)? start,
    TResult Function(DeliverLoadedEvent value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class DeliverLoadedEvent implements DeliverEvent {
  const factory DeliverLoadedEvent() = _$DeliverLoadedEvent;
}

/// @nodoc
class _$DeliverStateTearOff {
  const _$DeliverStateTearOff();

  _DeliverInitialState initial() {
    return const _DeliverInitialState();
  }

  _DeliverLoadingState loading() {
    return const _DeliverLoadingState();
  }

  _DeliverLoadedState loaded() {
    return const _DeliverLoadedState();
  }
}

/// @nodoc
const $DeliverState = _$DeliverStateTearOff();

/// @nodoc
mixin _$DeliverState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_DeliverInitialState value) initial,
    required TResult Function(_DeliverLoadingState value) loading,
    required TResult Function(_DeliverLoadedState value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_DeliverInitialState value)? initial,
    TResult Function(_DeliverLoadingState value)? loading,
    TResult Function(_DeliverLoadedState value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_DeliverInitialState value)? initial,
    TResult Function(_DeliverLoadingState value)? loading,
    TResult Function(_DeliverLoadedState value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeliverStateCopyWith<$Res> {
  factory $DeliverStateCopyWith(
          DeliverState value, $Res Function(DeliverState) then) =
      _$DeliverStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$DeliverStateCopyWithImpl<$Res> implements $DeliverStateCopyWith<$Res> {
  _$DeliverStateCopyWithImpl(this._value, this._then);

  final DeliverState _value;
  // ignore: unused_field
  final $Res Function(DeliverState) _then;
}

/// @nodoc
abstract class _$DeliverInitialStateCopyWith<$Res> {
  factory _$DeliverInitialStateCopyWith(_DeliverInitialState value,
          $Res Function(_DeliverInitialState) then) =
      __$DeliverInitialStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$DeliverInitialStateCopyWithImpl<$Res>
    extends _$DeliverStateCopyWithImpl<$Res>
    implements _$DeliverInitialStateCopyWith<$Res> {
  __$DeliverInitialStateCopyWithImpl(
      _DeliverInitialState _value, $Res Function(_DeliverInitialState) _then)
      : super(_value, (v) => _then(v as _DeliverInitialState));

  @override
  _DeliverInitialState get _value => super._value as _DeliverInitialState;
}

/// @nodoc

class _$_DeliverInitialState implements _DeliverInitialState {
  const _$_DeliverInitialState();

  @override
  String toString() {
    return 'DeliverState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _DeliverInitialState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() loaded,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? loaded,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_DeliverInitialState value) initial,
    required TResult Function(_DeliverLoadingState value) loading,
    required TResult Function(_DeliverLoadedState value) loaded,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_DeliverInitialState value)? initial,
    TResult Function(_DeliverLoadingState value)? loading,
    TResult Function(_DeliverLoadedState value)? loaded,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_DeliverInitialState value)? initial,
    TResult Function(_DeliverLoadingState value)? loading,
    TResult Function(_DeliverLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _DeliverInitialState implements DeliverState {
  const factory _DeliverInitialState() = _$_DeliverInitialState;
}

/// @nodoc
abstract class _$DeliverLoadingStateCopyWith<$Res> {
  factory _$DeliverLoadingStateCopyWith(_DeliverLoadingState value,
          $Res Function(_DeliverLoadingState) then) =
      __$DeliverLoadingStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$DeliverLoadingStateCopyWithImpl<$Res>
    extends _$DeliverStateCopyWithImpl<$Res>
    implements _$DeliverLoadingStateCopyWith<$Res> {
  __$DeliverLoadingStateCopyWithImpl(
      _DeliverLoadingState _value, $Res Function(_DeliverLoadingState) _then)
      : super(_value, (v) => _then(v as _DeliverLoadingState));

  @override
  _DeliverLoadingState get _value => super._value as _DeliverLoadingState;
}

/// @nodoc

class _$_DeliverLoadingState implements _DeliverLoadingState {
  const _$_DeliverLoadingState();

  @override
  String toString() {
    return 'DeliverState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _DeliverLoadingState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() loaded,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? loaded,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_DeliverInitialState value) initial,
    required TResult Function(_DeliverLoadingState value) loading,
    required TResult Function(_DeliverLoadedState value) loaded,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_DeliverInitialState value)? initial,
    TResult Function(_DeliverLoadingState value)? loading,
    TResult Function(_DeliverLoadedState value)? loaded,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_DeliverInitialState value)? initial,
    TResult Function(_DeliverLoadingState value)? loading,
    TResult Function(_DeliverLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _DeliverLoadingState implements DeliverState {
  const factory _DeliverLoadingState() = _$_DeliverLoadingState;
}

/// @nodoc
abstract class _$DeliverLoadedStateCopyWith<$Res> {
  factory _$DeliverLoadedStateCopyWith(
          _DeliverLoadedState value, $Res Function(_DeliverLoadedState) then) =
      __$DeliverLoadedStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$DeliverLoadedStateCopyWithImpl<$Res>
    extends _$DeliverStateCopyWithImpl<$Res>
    implements _$DeliverLoadedStateCopyWith<$Res> {
  __$DeliverLoadedStateCopyWithImpl(
      _DeliverLoadedState _value, $Res Function(_DeliverLoadedState) _then)
      : super(_value, (v) => _then(v as _DeliverLoadedState));

  @override
  _DeliverLoadedState get _value => super._value as _DeliverLoadedState;
}

/// @nodoc

class _$_DeliverLoadedState implements _DeliverLoadedState {
  const _$_DeliverLoadedState();

  @override
  String toString() {
    return 'DeliverState.loaded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _DeliverLoadedState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() loaded,
  }) {
    return loaded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? loaded,
  }) {
    return loaded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_DeliverInitialState value) initial,
    required TResult Function(_DeliverLoadingState value) loading,
    required TResult Function(_DeliverLoadedState value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_DeliverInitialState value)? initial,
    TResult Function(_DeliverLoadingState value)? loading,
    TResult Function(_DeliverLoadedState value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_DeliverInitialState value)? initial,
    TResult Function(_DeliverLoadingState value)? loading,
    TResult Function(_DeliverLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _DeliverLoadedState implements DeliverState {
  const factory _DeliverLoadedState() = _$_DeliverLoadedState;
}
