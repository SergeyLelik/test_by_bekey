import 'dart:html';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'deliver_bloc.freezed.dart';

@freezed
class DeliverEvent with _$DeliverEvent {
  const factory DeliverEvent.start() = DeliverStartEvent;
  const factory DeliverEvent.loaded() = DeliverLoadedEvent;
}

@freezed
class DeliverState with _$DeliverState {
  const factory DeliverState.initial() = _DeliverInitialState;
  const factory DeliverState.loading() = _DeliverLoadingState;
  const factory DeliverState.loaded() = _DeliverLoadedState;
}

class DeliverBloc extends Bloc<DeliverEvent, DeliverState> {
  DeliverBloc() : super(const DeliverState.initial());
}
