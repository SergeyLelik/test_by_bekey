import 'dart:convert';

import 'package:chopper/chopper.dart';

part 'deliver_api_provider.chopper.dart';

@ChopperApi(baseUrl: '/products')
abstract class DeliverApiProvider extends ChopperService {
  @Get()
  Future<Response> getPosts();

  @Get(path: '/{id}')
  Future<Response> getPost(@Path('id') int id);

  static DeliverApiProvider create() {
    final client = ChopperClient(
      baseUrl: 'https://fakestoreapi.com',
      services: [
        _$DeliverApiProvider(),
      ],
      converter: const JsonConverter(),
    );

    return _$DeliverApiProvider(client);
  }
}
