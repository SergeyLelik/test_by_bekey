import 'dart:convert';
import 'dart:developer';
import './model/deliver.dart';
import './service/deliver_api_provider.dart';

void main() async {
  final service = DeliverApiProvider.create();
  // final responce = await service.getPost(2);

  for (var i = 1; i < 30; i++) {
    final resp = await service.getPost(i);

    try {
      final deliver = Deliver.fromJson(json.decode(resp.bodyString));

      log(deliver.toString());
    } catch (e) {}
  }
}
